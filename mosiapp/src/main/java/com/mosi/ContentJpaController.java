/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mosi;

import com.mosi.jpa.Content;
import com.mosi.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author eg
 */
public class ContentJpaController implements Serializable {

    public ContentJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Content content) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(content);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Content content) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            content = em.merge(content);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = content.getId();
                if (findContent(id) == null) {
                    throw new NonexistentEntityException("The content with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Content content;
            try {
                content = em.getReference(Content.class, id);
                content.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The content with id " + id + " no longer exists.", enfe);
            }
            em.remove(content);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Content> findContentEntities() {
        return findContentEntities(true, -1, -1);
    }

    public List<Content> findContentEntities(int maxResults, int firstResult) {
        return findContentEntities(false, maxResults, firstResult);
    }

    private List<Content> findContentEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Content.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
/*
    protected List<Content> findContentEntities(String subject) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Content.findByTitle").setParameter("subject", subject);
            q.setMaxResults(100);
            q.setFirstResult(0);
            
            return q.getResultList();
        } finally {
            em.close();
        }
    }
*/
    public Content findContent(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Content.class, id);
        } finally {
            em.close();
        }
    }

    public int getContentCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Content> rt = cq.from(Content.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<Content> findContentByTitle(String title) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Content.findByTitle").setParameter(1, title).setParameter(2, title);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public void incementClicks(long cId) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            Query q = em.createNamedQuery("Points.incrementClicks").setParameter(1, cId);
            try {
                q.executeUpdate();
            } catch (Exception ex) {
                ex.printStackTrace(System.err);
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
            em.getTransaction().commit();
       } finally {
            em.close();
        }
    }
    
}
