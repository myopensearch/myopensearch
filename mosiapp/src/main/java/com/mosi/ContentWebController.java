/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mosi;

import com.mosi.web.WebContent;
import com.mosi.web.WebSearch;
import java.util.List;

/**
 *
 * @author eg
 */
class ContentWebController {
     
    private WebSearch webSearch;
    private ContentController mainController;

    public ContentWebController() {
         webSearch = new WebSearch("http://www.bing.com/search?q=");
   }

    ContentWebController(ContentController main) {
        this();
        this.mainController = main;
    }

    
    List<WebContent> findContentEntities() {
        return webSearch.search(mainController.getSearchSubject());
    }
    
}
