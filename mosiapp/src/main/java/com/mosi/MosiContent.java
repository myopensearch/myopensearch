/**
 * 
 */
package com.mosi;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author eg
 *
 */
public class MosiContent implements Serializable 
{
    private static final long serialVersionUID = 1L;

    private String title = null;

    private String description = null;
    
    private String url = null;
    
    private final static AtomicLong idSequence = new AtomicLong();
    
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public MosiContent(final String title,
                   final String description,
                   final String url
    )
    {
        this.id = (int)idSequence.getAndIncrement();
        this.url = url;
        this.title = title;
        this.description = description;
    }

    public MosiContent(final int id,
                   final String title,
                   final String description,
                   final String url
    )
    {
        this.id = id;
        this.url = url;
        this.title = title;
        this.description = description;
    }

    public MosiContent()
    {
        id = (int)idSequence.getAndIncrement();
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
    
    public boolean empty(){
        
        return getDescription() == null && getTitle() == null && getUrl() == null;
    }
}
