package com.mosi;

import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import static com.mosi.jpa.JpaContentBuilder.toContent;
import static com.mosi.jpa.JpaContentBuilder.toMosiContent;
import com.mosi.util.JsfUtil;
import com.mosi.util.JsfUtil.PersistAction;
import com.mosi.web.WebContentBuilder;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

@ManagedBean(name = "contentController")
@SessionScoped
public class ContentController implements Serializable {

    private ContentJpaController jpaController = null;
    private ContentWebController webController = null;
    private List<MosiContent> items = null;
    private List<MosiContent> extItems = null;
    private MosiContent selected = null;
    private MosiContent selectedExt = null;
    private MosiContent toShow = null;
    public boolean selectedEmpty = true;

    public boolean isSelectedEmpty() {
        return selectedEmpty;
    }

    public void setSelectedEmpty(boolean selectedEmpty) {
        this.selectedEmpty = selectedEmpty;
    }

    public MosiContent getToShow() {
        return toShow;
    }

    public void setToShow(MosiContent toShow) {
        this.toShow = toShow;
    }
    
    private static final ConcurrentSkipListSet<String> cachedQueries = new ConcurrentSkipListSet<String>();
    /*
    private static final LoadingCache<String, ArrayList<String>> cachedQueries = CacheBuilder.newBuilder()
       .build(
           new CacheLoader<String, ArrayList<String>>() {

        @Override
        public ArrayList<String> load(String k) throws Exception {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    });
    */
    
    public static final String FORWARD_SUCCESS = "SUCCESS";
    public static final String FORWARD_FAILURE = "FAILURE";
    private final AtomicBoolean started = new AtomicBoolean(false);

    private final AtomicLong invocationCount = new AtomicLong();
    private final AtomicLong searchCount = new AtomicLong();

    public ContentController() {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "invocation count: {0}", invocationCount.getAndIncrement());
        prepare();
    }

    public MosiContent getSelected() {
        return selected;
    }

    public void setSelected(MosiContent selected) {
        this.selected = selected;
        setSelectedEmpty(getSelected() == null || getSelected().empty());
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private synchronized ContentJpaController getJpaController() {
        if (jpaController == null) {
            jpaController = new ContentJpaController(EMF.getEmf());
        }
        return jpaController;
    }

    private synchronized ContentWebController getWebController() {
        if(webController == null){
            webController = new ContentWebController(this);
        }
        return webController;
    }

    public MosiContent prepareCreate() {
        setSelected( new MosiContent() );
        initializeEmbeddableKey();
        return getSelected();
    }

    public void create() {
        persist(getSelected(), PersistAction.CREATE, "ContentCreated");
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        checkSelectedUrl();
        persist(getSelected(), PersistAction.UPDATE, "ContentUpdated");
    }

    public void destroy() {
        persist(getSelected(), PersistAction.DELETE, "ContentDeleted");
        if (!JsfUtil.isValidationFailed()) {
            setSelected( null ); // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<MosiContent> getExtItems() {
        if (extItems == null) {
            extItems = WebContentBuilder.toMosiContent(
                    getWebController().findContentEntities());
        }
        return extItems;
    }

    public List<MosiContent> getItems() {
        if (items == null) {
            items = toMosiContent(
                    getJpaController().findContentByTitle(searchSubject));
        }
        return items;
    }

    private void persist(MosiContent content, PersistAction persistAction, String successMessage) {
        if (content != null) {
            setEmbeddableKeys();
            try {
                if (persistAction == PersistAction.UPDATE) {
                    getJpaController().edit(toContent(content));
                } else if (persistAction == PersistAction.CREATE) {
                    getJpaController().create(toContent(content));
                } else {
                    getJpaController().destroy(content.getId());
                }
                JsfUtil.addSuccessMessage(content.getTitle(), successMessage);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            }
        }
    }

    public List<MosiContent> getItemsAvailableSelectMany() {
        return toMosiContent(getJpaController().findContentEntities());
    }

    public List<MosiContent> getItemsAvailableSelectOne() {
        return toMosiContent(getJpaController().findContentEntities());
    }

    private void add2CachedQueries(String searchSubject) {
        if(searchSubject != null && searchSubject.length() > 1)
            cachedQueries.add(searchSubject);
        
        if(cachedQueries.size() > 10000)
            cachedQueries.pollFirst();
    }

    private synchronized void prepare() {
        if(items != null)
            items = null;
        if(extItems != null)
            extItems = null;
        if(!started.get()){
            started.set(true);
            int cc = getJpaController().getContentCount();
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "content count: {0}", cc);
            getWebController();
        }
    }

    public MosiContent getSelectedExt() {
        return selectedExt;
    }

    public void setSelectedExt(MosiContent selectedExt) {
        this.selectedExt = selectedExt;
    }
/*
    @FacesConverter(forClass = MosiContent.class)
    public static class ContentControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ContentController controller = (ContentController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "contentController");
            return toMosiContent(controller.getJpaController().findContent(getKey(value)));
        }

        Long getKey(String value) {
            Long key;
            key = Long.valueOf(value);
            return key;
        }

        String getStringKey(Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof MosiContent) {
                MosiContent o = (MosiContent) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Content.class.getName()});
                return null;
            }
        }

    }
*/
    private volatile String searchSubject;

    /**
     * Get the value of searchSubject
     *
     * @return the value of searchSubject
     */
    public String getSearchSubject() {
            return searchSubject;
    }

    /**
     * Set the value of searchSubject
     *
     * @param searchSubject new value of searchSubject
     */
    public void setSearchSubject(String searchSubject) {
        if(searchSubject != null && searchSubject.length() > 1)
        {
            this.searchSubject = searchSubject;
            add2CachedQueries(searchSubject);
        }
        else 
        {
            this.searchSubject = null;
        }
    }
    
    public List<String> completeText(String query) {
        
        setSearchSubject(query);

        Collection<String> filtered = Collections2.filter(cachedQueries,
        Predicates.containsPattern(query));    
    
        if(filtered instanceof List)
            return (List)filtered;
        else 
            return new CopyOnWriteArrayList<String>(filtered);
    }
     
    public String search(){
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "search count: {0}, search subject: {1}", new Object[]{searchCount.getAndIncrement(), getSearchSubject()});
        prepare();
        return FORWARD_SUCCESS;
    }

    public void onExtRowDblClckSelect(final SelectEvent event) {
        MosiContent obj = (MosiContent) event.getObject();
        createContent(obj);
        navigateToShow();
    }

    public void onRowDblClckSelect(final SelectEvent event) {
        MosiContent obj = (MosiContent) event.getObject();
        incementClicks(obj);
        setToShow(obj);
        navigateToShow();
    }

    private void navigateToShow(){
        ConfigurableNavigationHandler configurableNavigationHandler =
             (ConfigurableNavigationHandler)FacesContext.
               getCurrentInstance().getApplication().getNavigationHandler();
        configurableNavigationHandler.performNavigation("Show?faces-redirect=true");    
    }
    
    public void onSelectedView() {
        incementClicks(getSelected());
    }
    
    public void onExtSelectedView() {
        createContent(getSelectedExt());
    }

    public void createContent(MosiContent obj){
        persist(obj, PersistAction.CREATE, "ContentCreated");
        items = null;
        setToShow(obj);
    }

    private void checkSelectedUrl() {
        if(getSelected()!=null && !getSelected().getUrl().startsWith("http"))
            getSelected().setUrl("http://" + getSelected().getUrl());
    }

    public String onGoHome(){
        setSearchSubject(null);
        setSelected(null);
        setSelectedExt(null);
        setToShow(null);
        return FORWARD_SUCCESS;
    }

    public void incementClicks(MosiContent obj) {
        getJpaController().incementClicks(obj.getId());
    }
}