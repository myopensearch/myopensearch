/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mosi.jpa;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author eg
 */
@Entity
@Table(name = "points")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Points.findAll", query = "SELECT p FROM Points p"),
    @NamedQuery(name = "Points.findById", query = "SELECT p FROM Points p WHERE p.id = :id"),
    @NamedQuery(name = "Points.findByCId", query = "SELECT p FROM Points p WHERE p.cId = :cId"),
    @NamedQuery(name = "Points.findByClicks", query = "SELECT p FROM Points p WHERE p.clicks = :clicks")})
@NamedNativeQuery(name = "Points.incrementClicks", query = "UPDATE points SET clicks=clicks+1 WHERE c_id=?;")

public class Points implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "c_id")
    private long cId;
    @Column(name = "clicks")
    private long clicks;

    public Points() {
    }

    public Points(Integer id) {
        this.id = id;
    }

    public Points(Integer id, long cId, long clicks) {
        this.id = id;
        this.cId = cId;
        this.clicks = clicks;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public long getCId() {
        return cId;
    }

    public void setCId(long cId) {
        this.cId = cId;
    }

    public long getClicks() {
        return clicks;
    }

    public void setClicks(long clicks) {
        this.clicks = clicks;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Points)) {
            return false;
        }
        Points other = (Points) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mosi.jpa.Points[ id=" + id + " ]";
    }
    
}
