/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mosi.jpa;

import com.mosi.MosiContent;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author eg
 */
public class JpaContentBuilder {
    

    public static MosiContent toMosiContent(Content wc)
    {
        MosiContent c = toMosiContent(wc.getId(), wc.getTitle(), wc.getDescription(), wc.getUrl());
        return c;
    }

    public static Content toContent(MosiContent mc)
    {
        Content c = new Content(mc.getId(), mc.getTitle(), mc.getDescription(), mc.getUrl());
        return c;
    }

    public static MosiContent toMosiContent(int id, String title, String description, String url)
    {
        MosiContent c = new MosiContent(id , title, description, url);
        return c;
    }

    public static List<MosiContent> toMosiContent(List<Content> cList)
    {
        List<MosiContent> mcList = new ArrayList<MosiContent>();
        for(int i = 0; i < cList.size(); i++)
        {
            Object c = cList.get(i);
            if(c instanceof Content)
                mcList.add(toMosiContent((Content)c));
            if(c instanceof Object[])
                mcList.add(toMosiContent((Integer)((Object[])c)[0],(String)((Object[])c)[1],(String)((Object[])c)[2],(String)((Object[])c)[3]));

        }
        return mcList;
    }
}
