/**
 * 
 */
package com.mosi.web;

import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.lang.StringEscapeUtils;

/**
 * @author eg
 *
 */
public class WebContent
{
    private final String BR = "<br />";
    private final String STRONG_B = "<strong>";
    private final String STRONG_E = "</strong>";
    
    private String title;

    private String description;
    
    private String url;
    
    private final static AtomicLong idSequence = new AtomicLong();
    
    private final int id;

    public int getId() {
        return id;
    }

    public WebContent(final String url,
                   final String title,
                   final String description)
    {
        this();
        this.url = url;
        this.title = title;
        this.description = description;
    }

    public WebContent(final String title)
    {
        this();
        this.title = title;
    }

    public WebContent()
    {
        id = (int)idSequence.getAndIncrement();
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title.replaceAll(STRONG_B, "").replaceAll(STRONG_E, "");
        this.title = StringEscapeUtils.unescapeHtml(this.title);
    }

    public String getDescription()
    {
        return description;
    }

    public String getDbDescription()
    {
//        return description.replaceAll(STRONG_B, "").replaceAll(STRONG_E, "");
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description.replaceAll(STRONG_B, "").replaceAll(STRONG_E, "");
        this.description = StringEscapeUtils.unescapeHtml(this.description);
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
    
    public String toString(){
        return "\r\n\t" + title+ "\r\n\t" + description+ "\r\n\t" + url +"\r\n\t";
    }

    public void reset()
    {
        setTitle(null);
        setDescription(null);
        setUrl(null);
    }
    
    public String toShowHtml()
    {
        return getTitle()+ BR + getDescription() + BR + getAddUrl();
    }

    public String getLastAddedUrl()
    {
        return "<a href=\"show.jsp?url="+url +"&title="+title+"&description="+description+"\">"+title +"</a>";
    }

    public String getLastFoundUrl()
    {
        return "<a href=\"search.jsp?key="+title+"\">"+title +"</a>";
    }

    private String getAddUrl()
    {
        return "<a href=\"show.jsp?url="+url +"&title="+title+"&description="+description+"\">"+url +"</a>";
    }
    
    public String toHtmlForSearchKey(final String key)
    {
        return getTitle()+ BR + getDescription() + BR + getAddUrlForSearchKey(key);
    }

    private String getAddUrlForSearchKey(final String key)
    {
        return "<a href=\"add.jsp?key="+key +"&url="+url +"&title="+title+"&description="+description+"\">"+url +"</a>";
    }

}
