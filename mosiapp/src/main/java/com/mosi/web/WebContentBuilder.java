/**
 * 
 */
package com.mosi.web;

import com.mosi.MosiContent;
import java.util.ArrayList;
import java.util.List;

/**
 * @author eg
 *
 */
public class WebContentBuilder
{
    private final static String URL_SEARCH_PREF = "<a href=\"http";
    private final static String URL_SEARCH_SUFF = "\"";
    private final static String TITLE_SEARCH_PREF = "\">";
    private final static String TITLE_SEARCH_SUFF = "</a>";
    private final static String DESCRIPTION_SEARCH_PREF = "<p>";
    private final static String DESCRIPTION_SEARCH_SUFF = "</p>";
    private final static String SPAN_SEARCH_PREF = "<span";
    private final static String SPAN_SEARCH_SUFF = "</span>&nbsp;&#0183;&#32;";
    private final static String TITLE_ARG_SEARCH_PREF = "title=\"";
    private final static String TITLE_ARG_SEARCH_SUFF = "\"";
    
    
    public static WebContent fromHtml(final StringBuilder buffer)
    {
        WebContent content = getContentInstance();
        //search for url
        int beg = buffer.indexOf(URL_SEARCH_PREF);
        int end = -1;
        boolean parsed = true;
        if(beg > 0)
        {
            beg+=(URL_SEARCH_PREF.length()-4);
            end = buffer.indexOf(URL_SEARCH_SUFF, beg);
            if(end > 0){
                content.setUrl(buffer.substring(beg, end));
                
                //search for title
                beg = buffer.indexOf(TITLE_SEARCH_PREF,end);
                if(beg > 0)
                {
                    beg+=(TITLE_SEARCH_PREF.length());
                    end = buffer.indexOf(TITLE_SEARCH_SUFF, beg);
                    if(end > 0)
                    {
                        String title = buffer.substring(beg, end);
                        title = toNoTitleArg(title);
                        content.setTitle(title);
                
                        //search for description
                        beg = buffer.indexOf(DESCRIPTION_SEARCH_PREF,end);
                        if(beg > 0)
                        {
                            beg+=(DESCRIPTION_SEARCH_PREF.length());
                            end = buffer.indexOf(DESCRIPTION_SEARCH_SUFF, beg);
                            if(end > 0)
                            {
                                String descr = buffer.substring(beg, end);
                                //descr = toNoSpan(descr);
                                content.setDescription(descr);
                            }else
                                parsed = false;
                        }else
                            parsed = false;
                    }
                }else
                    parsed = false;
            }else
                parsed = false;
        }else
            parsed = false;
        if(parsed)
            return content;
        else{
            reuseContentInstance(content);
            return null;
        }
        
    }


    private static void reuseContentInstance(WebContent content)
    {
        // TODO Auto-generated method stub
        
    }


    private static WebContent getContentInstance()
    {
        /*
        WebContent c = pool.remove(0);
        if(c == null)
            c = new WebContent();
        else
            c.reset();
            
        return c;
        */
        return new WebContent();
    }

    public static MosiContent toMosiContent(WebContent wc)
    {
        MosiContent c = new MosiContent(wc.getId(), wc.getTitle(), wc.getDescription(), wc.getUrl());
        return c;
    }

    public static List<MosiContent> toMosiContent(List<WebContent> wcList)
    {
        if(wcList == null)
            return null;
        
        List<MosiContent> mcList = new ArrayList<MosiContent>();
        for(WebContent wc : wcList)
        {
            if( (wc.getUrl() != null && wc.getUrl().length() > 4 
                    && wc.getUrl().length() <= 512 && !wc.getUrl().contains(".r.msn.com"))
                    && (!wc.getDescription().contains(SPAN_SEARCH_PREF)))
                mcList.add(toMosiContent(wc));
        }
        return mcList;
    }

    private static String toNoSpan(String descr) {
        int beg = descr.indexOf(SPAN_SEARCH_PREF);
        if(beg >= 0)
            beg+= SPAN_SEARCH_PREF.length();
        int end = descr.indexOf(SPAN_SEARCH_SUFF);
        if(end >= 0)
            end+= SPAN_SEARCH_SUFF.length();
        if(beg >= 0 && end > beg){
            descr = descr.substring(end);
        }
        return descr.trim();
    }

    private static String toNoTitleArg(String descr) {
        int beg = descr.indexOf(TITLE_ARG_SEARCH_PREF);
        if(beg >= 0)
            beg+= TITLE_ARG_SEARCH_PREF.length();
        else
            return descr;
        int end = descr.indexOf(TITLE_ARG_SEARCH_SUFF, beg);
        if(end > beg){
            descr = descr.substring(beg, end);
        }
        return descr.trim();
    }
}
