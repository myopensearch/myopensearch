package com.mosi.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringEscapeUtils;

public class WebSearch
{

    final private String searchUrl;
    private Proxy        proxy = null;

    public WebSearch(String searchUrl)
    {
        super();
        this.searchUrl = searchUrl;
        createProxy();
    }

    private void createProxy()
    {
    	String proxyHost = System.getProperty("proxyHost");
    	if(proxyHost != null && !proxyHost.isEmpty()) {
            SocketAddress addr = new InetSocketAddress(proxyHost, 8080);
            proxy = new Proxy(Proxy.Type.HTTP, addr);
    	}
    }

    private List<WebContent> doSearch(final URLConnection conn) throws IOException
    {
        final List<WebContent> result = new ArrayList<WebContent>();
        // submit the search

//        final InputStream is = conn.getInputStream();
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8")); 
//        final ParseHTML parse = new ParseHTML(is);
        final StringBuilder buffer = new StringBuilder();
        boolean capture = false;

        // parse the results
        int ch;
        StringBuilder lastRead = new StringBuilder();
        final String tag = "li";
        while ((ch = is.read()) != -1)
        {

            if (ch == 0)
            {
                /*
                final HTMLTag tag = parse.getTag();
                System.err.println(tag);
                if (tag.getName().equalsIgnoreCase("a"))
                {
//                    buffer.setLength(0);
//                    capture = true;
                }
                else if (tag.getName().equalsIgnoreCase("/a"))
                {
                    //result.add(new URL(buffer.toString()));
                    //result.add(parseContent(buffer.toString()));
//                    buffer.setLength(0);
//                    capture = false;
                }
                */
            }
            else
            {
                if(lastRead.length() > 10)
                    lastRead.deleteCharAt(0);
                lastRead.append((char)ch);
                if(lastRead.toString().endsWith("<"+tag) && !capture)
                {
                    buffer.setLength(0);
                    capture = true;
                    
                }else                
                if(lastRead.toString().endsWith("</"+tag+">") && capture)
                {
                    WebContent content = null; 
                    content = parseContent(buffer);
                    if(content != null)
                        result.add(content);
                    buffer.setLength(0);
                    capture = false;
                }
                if (capture)
                {
                    buffer.append((char) ch);
                }
            }
        }
        return result;
    }

    private WebContent parseContent(final StringBuilder buffer)
    {
        WebContent content = WebContentBuilder.fromHtml(buffer);
        return content;
    }

    /**
     * Called to extract a list from the specified URL.
     * 
     * @param searchFor
     * @return 
     */
    public List<WebContent> search(final String searchFor)
    {
        try {
            List<WebContent> result = null;
            String connStr = "";
            for (StringTokenizer stringTokenizer = new StringTokenizer(searchFor, " "); stringTokenizer.hasMoreTokens();) {
                String token = stringTokenizer.nextToken().trim();
                if(token.length() == 0)
                    continue;
                token = URLEncoder.encode(token, "UTF-8");
                if(connStr.length() > 0)
                    connStr += ("+"+token);
                else
                    connStr = token;
            }
            //connStr = StringEscapeUtils.escapeHtml(connStr);
            String urlStr = searchUrl + connStr;
            System.out.println("urlStr: " + urlStr);
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "web search for: {0}", urlStr);
            final URL url = new URL(urlStr);
            URLConnection conn = null;
            if(proxy != null)
             conn = url.openConnection(proxy);
            else
             conn = url.openConnection();
            
            int tries = 0;
            boolean done = false;
            while (!done)
            {
                try
                {
                    result = doSearch(conn);
                    done = true;
                }
                catch (final IOException e)
                {
                    if (tries == 5)
                    {
                        throw (e);
                    }
                    try
                    {
                        Thread.sleep(2000);
                    }
                    catch (final InterruptedException e1)
                    {
                    }
                }
                tries++;
            }

            return result;
        } catch (MalformedURLException ex) {
            Logger.getLogger(WebSearch.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WebSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

}