SELECT * FROM content WHERE MATCH (title, description) AGAINST ('database');

SELECT * FROM content WHERE MATCH (title, description) AGAINST ('Tutorial');

SELECT * FROM content WHERE MATCH (title, description) AGAINST ('configured');

SELECT * FROM content WHERE MATCH (title, description) AGAINST ('mosi');
SELECT * FROM content WHERE MATCH (title, description) AGAINST ('MyOpenSearchInfo' WITH QUERY EXPANSION);

select * from content

select * from points

SHOW VARIABLES

SELECT *, MATCH (title, description) AGAINST ('mysql') FROM content;

SELECT *, MATCH (title,description) AGAINST
    ('Security implications of running MySQL as root') AS score
    FROM content WHERE MATCH (title,description) AGAINST
    ('Security implications of running MySQL as root');

SELECT *, MATCH (title,description) AGAINST ('MySQL') as score from content c, points p where c.id = p.c_id 
and MATCH (title,description) AGAINST ('MySQL') order by (p.clicks / 100 + score) desc;


update points set clicks = 100 where c_id = 106;


SHOW VARIABLES LIKE "%version%";

--STATUS;