/*CREATE DATABASE mosi;*/
CREATE DATABASE mosi DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

CREATE USER mosijobson IDENTIFIED BY 'swordfish';

GRANT select, insert, update, delete ON mosi.* TO 'mosijobson'@'%';

CREATE TABLE mosi.content (id INT(11) NOT NULL AUTO_INCREMENT,  title text NOT NULL, description text NOT NULL,  url VARCHAR(512) NOT NULL, PRIMARY KEY (id) );

ALTER TABLE mosi.content ENGINE = MyISAM ;

ALTER TABLE `mosi`.`content`
drop INDEX `FULLTEXT`;

CREATE FULLTEXT INDEX `FULLTEXT` on `mosi`.`content` (`title` ASC, `description` ASC);

ALTER TABLE `mosi`.`content` MODIFY description text NOT NULL;

ALTER TABLE `mosi`.`content` change subject title text NOT NULL;
commit;


SELECT url, title, description FROM mosi.CONTENT WHERE MATCH (title, description) AGAINST ('find' WITH QUERY EXPANSION);

SELECT url, title, description FROM mosi.CONTENT WHERE MATCH (title, description) AGAINST ('find');

INSERT INTO mosi.CONTENT (title,description,url) VALUES
    ('MySQL Tutorial','DBMS stands for DataBase ...','www.oracle.com'),
    ('How To Use MySQL Well','After you went through a ...','www.oracle.com'),
    ('Optimizing MySQL','In this tutorial we will show ...','www.oracle.com'),
    ('1001 MySQL Tricks','1. Never run mysqld as root. 2. ...','www.oracle.com'),
    ('MySQL vs. YourSQL','In the following database comparison ...','www.oracle.com'),
    ('MySQL Security','When configured properly, MySQL ...','www.oracle.com');
    
    
SELECT * FROM mosi.CONTENT WHERE MATCH (title,description) AGAINST ('database');

SELECT * FROM mosi.CONTENT WHERE MATCH (title,description) AGAINST ('find');
