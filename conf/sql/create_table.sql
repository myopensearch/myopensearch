
drop table mosi.points;
drop table mosi.content;

ALTER DATABASE mosi CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE mosi.content (
    id MEDIUMINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title TEXT NOT NULL,
    description TEXT,
    url VARCHAR(512) NOT NULL,
    FULLTEXT ( title , description )
);

CREATE TABLE mosi.points (
    id MEDIUMINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    c_id MEDIUMINT NOT NULL,
    clicks MEDIUMINT DEFAULT 1,
    update_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP on UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT FOREIGN KEY (c_id) REFERENCES content (id)
);
