drop trigger mosi.content_points_trigger;

CREATE TRIGGER mosi.content_points_trigger AFTER INSERT ON content
  FOR EACH ROW INSERT INTO points SET c_id = NEW.id;

CREATE TRIGGER mosi.content_create_timestamp BEFORE INSERT ON content
  FOR EACH ROW SET create_timestamp = now();

